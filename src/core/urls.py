"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.http import HttpResponseRedirect

urlpatterns = [
    path('admin/', admin.site.urls),
]

handler404 = 'core.urls._handler404'
handler500 = 'core.urls._handler500'


def _handler404(request, exception):
    return HttpResponseRedirect('//http.cat/404')


def _handler500(request):
    return HttpResponseRedirect('//http.cat/500')
